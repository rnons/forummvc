Inspired by [TodoMVC](http://todomvc.com), ForumMVC aims to implement a minimal forum with or without various MV* frameworks.

See it: <http://rnons.bitbucket.org/forummvc>

## Current Examples

- [AngularJS](http://rnons.bitbucket.org/forummvc/angularjs)
- [VanillaJS](http://rnons.bitbucket.org/forummvc/vanillajs)
- [Angular 2](http://rnons.bitbucket.org/forummvc/angular2)
- [React](http://rnons.bitbucket.org/forummvc/react)

## Run e2e tests

```
cd tests
./run.js --framework angularjs
```
