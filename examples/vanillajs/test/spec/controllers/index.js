/* global describe, it, beforeEach, expect, spyOn */

import IndexController from '../../../scripts/controllers/index.js';

let Mediator = {
  createTopic() {
    return Promise.resolve();
  }
};

let View = {
  bind() {},
  render() {}
};

let Topics = {
  all() {}
};

let indexController;

beforeEach(() => {
  indexController = new IndexController(Mediator, Topics, View);
  spyOn(Mediator, 'createTopic').and.callThrough();
});

describe('IndexController', function () {
  it('createTopic should be a function', () => {
    expect(typeof indexController.createTopic).toBe('function');
  });

  it('should not call Mediator.createTopic if title is empty', () => {
    indexController.createTopic('', '');
    expect(Mediator.createTopic).not.toHaveBeenCalled();
  });

  it('should not createTopic if title consists of only spaces', () => {
    indexController.createTopic('       ', '');
    expect(Mediator.createTopic).not.toHaveBeenCalled();
  });

  it('should trim title/body in createTopic', () => {
    let title = '   new topic    ';
    let body = '   good day    ';
    indexController.createTopic(title, body);
    expect(Mediator.createTopic).toHaveBeenCalledWith({
      title: 'new topic',
      body: 'good day'
    });
  });
});
