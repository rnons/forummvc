import IndexView from './views/index.js';
import TopicView from './views/topic.js';

export { IndexView, TopicView };
