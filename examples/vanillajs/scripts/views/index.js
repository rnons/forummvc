import Utils from './utils.js';

export default class View {
  constructor() {
    this.$topicForm = document.getElementById('topicForm');
    this.$topicTitle = document.getElementById('topicTitle');
    this.$topicBody = document.getElementById('topicBody');
    this.$topicList = document.getElementById('topicList');
    this.template = '<div class="Topic"><a href="#/topics/{{id}}">{{title}}</a></div>';
  }

  bind(event, handler) {
    if (event === 'newTopic') {
      this.$topicForm.addEventListener('submit', (event) => {
        event.preventDefault();
        handler(this.$topicTitle.value, this.$topicBody.value);
      });
    }
  }

  showTopics(data) {
    let view = '';
    for(let key in data) {
      view += this.template.replace('{{title}}', Utils.escape(data[key].title))
                           .replace('{{id}}', data[key].id);
    }
    return view;
  }

  render(data) {
    this.$topicList.innerHTML = this.showTopics(data);
  }
}
