function select(selector, element=document) {
  return element.querySelector(selector);
}

function selectAll(selector, element=document) {
  return element.querySelectorAll(selector);
}

function delegate(target, selector, type, handler) {
  function dispatchEvent(event) {
    var targetElement = event.target;
    var potentialElements = selectAll(selector, target);
    var hasMatch = Array.prototype.indexOf.call(potentialElements, targetElement) !== -1;

    if (hasMatch) {
      handler.call(targetElement, event);
    }
  }

  target.addEventListener(type, dispatchEvent);
}

function getParentByClassName(element, className) {
  if (!element.parentNode || element === document.body) {
    return;
  }
  if (element.parentNode.classList.contains(className)) {
    return element.parentNode;
  }
  return getParentByClassName(element.parentNode, className);
}

function escape (str) {
  return str.replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;');
}


export default {
  select,
  selectAll,
  delegate,
  getParentByClassName,
  escape
};
