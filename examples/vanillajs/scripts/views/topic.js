import Utils from './utils.js';

export default class TopicView {
  constructor() {
    this.$commentForm = document.getElementById('commentForm');
    this.$topicTitle = document.getElementById('topicTitle');
    this.$topicBody = document.getElementById('topicBody');
    this.$commentBody = document.getElementById('commentBody');
    this.$tree = document.getElementById('comments');
    this.template = document.getElementById('comment.html').innerHTML;

    let self = this;
    Utils.delegate(this.$tree, 'input[data-action="cancelEdit"]', 'click', function() {

      self.resetEditing(self.getParentTreeItem(this));
    });

    Utils.delegate(this.$tree, 'input[data-action="cancelReply"]', 'click', function() {

      self.resetReplying(self.getParentTreeItem(this));
    });
  }

  getParentTreeItem(element) {
    return Utils.getParentByClassName(element, 'Comment');
  }

  getTreeItemById(id) {
    return Utils.select(`div[data-id="${id}"]`);
  }

  setEditing(treeItem) {
    treeItem.classList.add('is-editing');
  }

  resetEditing(treeItem) {
    treeItem.classList.remove('is-editing');
  }

  setReplying(treeItem) {
    treeItem.classList.add('is-replying');
  }

  resetReplying(treeItem) {
    treeItem.classList.remove('is-replying');
  }

  bind(event, handler) {
    let self = this;
    if (event === 'saveComment') {
      this.$commentForm.addEventListener('submit', (event) => {
        event.preventDefault();
        handler(this.$commentBody.value);
      });
    } else if (event === 'toolClicked') {
      Utils.delegate(this.$tree, '.Tools a', 'click', function () {
        let treeItem = self.getParentTreeItem(this);
        handler(treeItem.dataset.id, this.dataset.action);
      });
    } else if (event === 'saveEdit') {
      Utils.delegate(this.$tree, 'input[data-action="saveEdit"]', 'click', function (event) {
        event.preventDefault();
        let treeItem = self.getParentTreeItem(this);
        let newBody = Utils.select('.EditForm textarea', treeItem).value;
        handler(treeItem.dataset.id, newBody);
      });
    } else if (event === 'saveReply') {
      Utils.delegate(this.$tree, 'input[data-action="saveReply"]', 'click', function (event) {
        event.preventDefault();
        let treeItem = self.getParentTreeItem(this);
        let body = Utils.select('.ReplyForm textarea', treeItem).value;
        handler(treeItem.dataset.id, body);
      });
    }
  }

  showTopic(topic) {
    this.$topicTitle.innerHTML = Utils.escape(topic.title);
    this.$topicBody.innerHTML = Utils.escape(topic.body);
  }

  showComment(comment, comments='') {
    return this.template.replace('{{id}}', comment.id)
                        .replace('{{body}}', Utils.escape(comment.body))
                        .replace('{{comments}}', comments);
  }

  showComments(data) {
    let view = '';
    for(let comment of data.comments) {
      let comments = '';
      if (comment.comments.length) {
        comments = this.showComments(comment);
      }
      view += this.showComment(comment, comments);
    }
    return view;
  }

  createItem(html) {
    let div = document.createElement('div');
    div.innerHTML = html;
    return div.children[0];
  }

  render(cmd, params) {
    let cmdMap = {
      all: () => {
        this.showTopic(params);
        this.$tree.innerHTML = this.showComments(params);
      },
      saveComment: () => {
        let newTreeItem = this.createItem(this.showComment(params));
        this.$tree.appendChild(newTreeItem);
      },
      edit: () => {
        let treeItem = this.getTreeItemById(params.id);
        Utils.select('.EditForm textarea', treeItem).value = params.body;
        this.setEditing(treeItem);
      },
      saveEdit: () => {
        let treeItem = this.getTreeItemById(params.id);
        this.resetEditing(treeItem);
        Utils.select('.Comment-body', treeItem).textContent = params.body;
      },
      delete: () => {
        let treeItem = this.getTreeItemById(params.id);
        treeItem.remove();
      },
      reply: () => {
        let treeItem = this.getTreeItemById(params.id);
        this.setReplying(treeItem);
      },
      saveReply: () => {
        let treeItem = this.getTreeItemById(params.parentId);
        let newTreeItem = this.createItem(this.showComment(params));
        treeItem.appendChild(newTreeItem);
        this.resetReplying(treeItem);
      }
    };
    cmdMap[cmd]();
  }
}
