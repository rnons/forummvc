export default class IndexController {
  constructor(Mediator, Topics, View) {
    this.Mediator = Mediator;
    this.Topics = Topics;
    this.View = View;

    this.View.bind('newTopic', (title, body) => {
      this.createTopic(title, body);
    });

    this.showAll();
  }

  createTopic(title, body) {
    let newTopic = {
      title: title.trim(),
      body: body.trim()
    };
    if (!newTopic.title) return;
    this.Mediator.createTopic(newTopic).then(() => {
      this.showAll();
    });
  }

  showAll() {
    this.View.render(this.Topics.all());
  }
}
