export default class TopicController {
  constructor(Comments, Mediator, Model, View) {
    this.Comments = Comments;
    this.Mediator = Mediator;
    this.Model = Model;
    this.View = View;

    this.View.bind('saveComment', (body) => {
      this.saveComment(body);
    });

    this.View.bind('toolClicked', (id, action) => {
      if (action === 'edit') {
          this.editItem(id);
      } else if (action === 'delete') {
          this.deleteItem(id);
      } else if (action === 'reply') {
          this.replyItem(id);
      }
    });

    this.View.bind('saveEdit', (id, newBody) => {
      this.saveEdit(id, newBody);
    });

    this.View.bind('saveReply', (id, body) => {
      this.saveReply(id, body);
    });

    this.showAll();
  }

  saveComment(body) {
    let newComment = {
      parentId: this.Model.id,
      parentType: 'topic',
      body: body.trim()
    };
    if (!newComment.body) return;

    this.Mediator.createComment(newComment).then((comment) => {
      this.View.render('saveComment', comment);
    });
  }

  editItem(id) {
    this.View.render('edit', this.Comments.getById(id));
  }

  saveEdit(id, newBody) {
    if (!newBody.trim()) return;

    let params = {};
    let comment = this.Comments.getById(id);
    for (let key in comment) {
      params[key] = comment[key];
    }
    params.body = newBody.trim();

    this.Mediator.updateComment(params).then((comment) => {
      this.View.render('saveEdit', comment);
    });
  }

  deleteItem(id) {
    let comment = this.Comments.getById(id);
    this.Mediator.deleteComment(comment).then(() => {
      this.View.render('delete', comment);
    });
  }

  replyItem(id) {
    this.View.render('reply', this.Comments.getById(id));
  }

  saveReply(id, body) {
    let newComment = {
      parentId: id,
      parentType: 'comment',
      body: body.trim()
    };
    if (!newComment.body) return;

    this.Mediator.createComment(newComment).then((comment) => {
      this.View.render('saveReply', comment);
    });
  }

  showAll() {
    this.View.render('all', this.Model);
  }
}
