import IndexController from './controllers/index.js';
import TopicController from './controllers/topic.js';

export { IndexController, TopicController };
