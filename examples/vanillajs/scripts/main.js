import { Comments, Mediator, Storage, Topics } from './models.js';
import { IndexView, TopicView } from './views.js';
import { IndexController, TopicController } from './controllers.js';

let comments = new Comments();
let storage = new Storage();
let topics = new Topics();
let mediator = new Mediator(comments, storage, topics);

let $container = document.getElementById('container');

function loadIndex () {
  mediator.fetch().then(() => {
    $container.innerHTML = document.getElementById('index.html').innerHTML;
    let indexView = new IndexView();

    new IndexController(mediator, topics, indexView);
  });
}

function loadTopic(id) {
  mediator.fetch().then(() => {
    $container.innerHTML = document.getElementById('topic.html').innerHTML;
    let topic = topics.topics[id];
    let topicView = new TopicView();

    new TopicController(comments, mediator, topic, topicView);
  });
}

function resolveRoute() {
  let hash = document.location.hash;
  let re = /^#\/topics\/([0-9.]+)/;
  let match;
  if (hash === '') {
    loadIndex();
  } else if ((match = re.exec(hash))) {
    loadTopic(match[1]);
  } else {
    loadIndex();
  }
}

resolveRoute();
window.addEventListener('hashchange', resolveRoute);
