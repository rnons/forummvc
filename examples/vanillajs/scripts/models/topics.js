class Topic {
  constructor(params) {
    this.id = params.id,
    this.title = params.title,
    this.body = params.body,
    this.comments = [];
  }

  addComment(comment) {
    this.comments.push(comment);
    return comment;
  }

  removeComment(comment) {
    this.comments.splice(this.comments.indexOf(comment), 1);
  }
}

export default class TopicProvider {
  constructor() {
    this.topics = {};
  }

  create(params) {
    const id = params.id ? params.id : Date.now() + Math.random();
    let topic = {
      id,
      title: params.title,
      body: params.body,
      comments: []
    };
    return Promise.resolve(topic);
  }

  add(params) {
    let topic = new Topic(params);
    this.topics[topic.id] = topic;
    return topic;
  }

  all() {
    return this.topics;
  }

  getById(id) {
    return this.topics[id];
  }
}
