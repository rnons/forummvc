import React from 'react';
import classNames from 'classnames';

class CommentForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    let body = React.findDOMNode(this.refs.body).value.trim();
    if (!body) return;
    let params = {
      parentId: this.props.parentId,
      parentType: this.props.parentType,
      body: body
    };
    this.props.mediator.createComment(params).then(() => {
      if (this.props.handleSubmit) {
        this.props.handleSubmit();
      }
    });
  }

  render() {
    let cancelButton = null;
    if (this.props.parentType === 'comment') {
      cancelButton = (
        <input type="button"
               className="Button Button--primary"
               onClick={this.props.handleCancel}
               value="Cancel"/>
      );
    }
    return (
      <form className="Form" {...this.props}
            onSubmit={this.handleSubmit}
            noValidate>
        <fieldset>
          <textarea className="Form-topic-body"
                    name="commentBody"
                    ref="body"
                    placeholder="Add a comment"
                    required></textarea>
        </fieldset>
        <fieldset>
          <input type="submit"
                 className="Button Button--primary"
                 value="Save"/>
          {cancelButton}
        </fieldset>
      </form>
    );
  }
}

class EditForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    let newBody = React.findDOMNode(this.refs.body).value.trim();
    if (!newBody) return;
    this.props.handleSubmit(newBody);
  }

  render() {
    return (
      <form className="Form" {...this.props}
            onSubmit={this.handleSubmit}
            noValidate>
        <fieldset>
          <textarea className="Form-topic-body"
                    name="commentBody"
                    ref="body"
                    placeholder="Add a comment"
                    defaultValue={this.props.data}
                    required></textarea>
        </fieldset>
        <fieldset>
          <input type="submit"
                 className="Button Button--primary"
                 value="Save"/>
          <input type="button"
                 className="Button Button--primary"
                 onClick={this.props.handleCancel}
                 value="Cancel"/>
        </fieldset>
      </form>
    );
  }
}

class Comment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editing: false,
      replying: false
    };
    this.setEditing = this.setEditing.bind(this);
    this.resetEditing = this.resetEditing.bind(this);
    this.deleteComment = this.deleteComment.bind(this);
    this.setReplying = this.setReplying.bind(this);
    this.resetReplying = this.resetReplying.bind(this);
    this.saveEdit = this.saveEdit.bind(this);
    this.saveReply = this.saveReply.bind(this);
  }

  setEditing() {
    this.setState({editing: true});
  }

  resetEditing() {
    this.setState({editing: false});
  }

  setReplying() {
    this.setState({replying: true});
  }

  resetReplying() {
    this.setState({replying: false});
  }

  saveEdit(newBody) {
    let params = {};
    for (let key in this.props.data) {
      params[key] = this.props.data[key];
    }
    params.body = newBody;
    this.props.mediator.updateComment(params).then(() => {
      this.resetEditing();
    });
  }

  saveReply() {
    this.resetReplying();
  }

  deleteComment() {
    this.props.handleDelete(this.props.data);
  }

  render() {
    let classes = classNames('Comment', {
      'is-editing': this.state.editing,
      'is-replying': this.state.replying
    });
    return (
      <div className={classes}>
        <div className="Comment-body">{this.props.data.body}</div>
        <EditForm className="EditForm"
                  data={this.props.data.body}
                  handleSubmit={this.saveEdit}
                  handleCancel={this.resetEditing}/>
        <div className="Comment-tools">
          <a onClick={this.setEditing}>edit</a>
          <a onClick={this.deleteComment}>delete</a>
          <a onClick={this.setReplying}>reply</a>
        </div>
        <CommentForm className="ReplyForm"
                     parentId={this.props.data.id}
                     parentType="comment"
                     mediator={this.props.mediator}
                     handleSubmit={this.saveReply}
                     handleCancel={this.resetReplying}/>
        <Comments comments={this.props.data.comments}
                  mediator={this.props.mediator}
                  handleDelete={this.props.handleDelete}/>
      </div>
    );
  }
}

class Comments extends React.Component {
  render() {
    let comments = this.props.comments.map((comment, index) => {
      return (
        <Comment data={comment}
                 key={index}
                 mediator={this.props.mediator}
                 handleDelete={this.props.handleDelete}/>
      );
    });
    return (
      <div>
        {comments}
      </div>
    );
  }
}
Comments.defaultProps = { comments: [] };

export class TopicPage extends React.Component {
  constructor() {
    super();
    this.state = {topic: {}};
    this.getTopic = this.getTopic.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    this.props.mediator.fetch().then(() => {
      this.getTopic();
    });
  }

  getTopic() {
    let topic = this.props.topics.getById(this.props.id);
    this.setState({topic: topic});
  }

  handleDelete(comment) {
    this.props.mediator.deleteComment(comment).then(() => {
      this.getTopic();
    });
  }

  render() {
    return (
      <div>
        <h2>{this.state.topic.title}</h2>
        <div>{this.state.topic.body}</div>
        <CommentForm parentId={this.state.topic.id}
                     parentType="topic"
                     mediator={this.props.mediator}
                     handleSubmit={this.getTopic}/>
        <Comments comments={this.state.topic.comments}
                  mediator={this.props.mediator}
                  handleDelete={this.handleDelete}/>
      </div>
    );
  }
}
