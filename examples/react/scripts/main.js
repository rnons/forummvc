import React from 'react';
import { Comments, Mediator, Storage, Topics } from './models.js';
import { Home } from './home.js';
import { TopicPage } from './topic.js';


let comments = new Comments();
let storage = new Storage();
let topics = new Topics();
let mediator = new Mediator(comments, storage, topics);

function loadHome() {
  React.render(
    <Home mediator={mediator} topics={topics}/>,
    document.getElementById('container')
  );
}

function loadTopic(id) {
  React.render(
    <TopicPage mediator={mediator} topics={topics} id={id}/>,
    document.getElementById('container')
  );
}

function resolveRoute() {
  let hash = document.location.hash;
  let re = /^#\/topics\/([0-9.]+)/;
  let match;
  if (hash === '') {
    loadHome();
  } else if ((match = re.exec(hash))) {
    loadTopic(match[1]);
  } else {
    loadHome();
  }
}

resolveRoute();
window.addEventListener('hashchange', resolveRoute);
