import React from 'react';

class TopicForm extends React.Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    let titleNode, bodyNode;
    titleNode = React.findDOMNode(this.refs.title);
    bodyNode = React.findDOMNode(this.refs.body);
    let newTopic = {
      title: titleNode.value.trim(),
      body: bodyNode.value.trim()
    };
    if (!newTopic.title) return;
    this.props.onSubmit(newTopic);
    titleNode.value = '';
    bodyNode.value = '';
  }

  render() {
    return (
      <form className="Form"
            id="topicForm"
            onSubmit={this.handleSubmit}
            noValidate>
        <fieldset>
          <input type="text"
                 className="Form-topic-title"
                 id="topicTitle"
                 ref="title"
                 name="title"
                 placeholder="Post a new topic"
                 required/>
        </fieldset>
        <fieldset>
          <textarea className="Form-topic-body"
                    id="topicBody"
                    ref="body"
                    placeholder="Content"></textarea>
        </fieldset>
        <fieldset>
          <input type="submit"
                 className="Button Button--primary"
                 value="Create"/>
        </fieldset>
      </form>
    );
  }
}

class TopicList extends React.Component {
  constructor() {
    super();
  }

  render() {
    let topicNodes = this.props.topics.map((topic, index) => {
      return (<div className="Topic" key={index}>
          <a href={`#/topics/${topic.id}`}>{topic.title}</a>
        </div>
      );
    });
    return (
      <div>
        {topicNodes}
      </div>
    );
  }
}

export class Home extends React.Component {
  constructor() {
    super();
    this.state = {topics: []};
    this.getTopics = this.getTopics.bind(this);
    this.createTopic = this.createTopic.bind(this);
  }

  componentDidMount() {
    this.props.mediator.fetch().then(() => {
      this.getTopics();
    });
  }

  getTopics() {
    let data = [];
    let topics = this.props.topics.all();
    for (let i in topics) {
      data.push(topics[i]);
    }
    this.setState({topics: data});
  }

  createTopic(newTopic) {
    this.props.mediator.createTopic(newTopic).then(() => {
      this.getTopics();
    });
  }

  render() {
    return (
      <div>
        <h2>Create a new topic</h2>
        <TopicForm onSubmit={this.createTopic}/>
        <h2>Topics</h2>
        <div id="topicList"></div>
        <TopicList topics={this.state.topics}/>
      </div>
    );
  }
}
