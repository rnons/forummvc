import {Component, CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/angular2';

import {Comments} from '../../services/comments';
import {Mediator} from '../../services/mediator';

@Component({
  selector: 'edit-form',
  templateUrl: './components/comment_form/comment_form.html',
  directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
  properties: ['comment']
})
export class EditForm {
  constructor(Comments, Mediator) {
    this.Comments = Comments;
    this.Mediator = Mediator;
    this._comment = null;
    this.model = {
      body: ''
    };
  }

  get comment() {
    return this._comment;
  }

  set comment(newVal) {
    this._comment = newVal;
    this.model.body = this.comment.body;
  }

  showCancel() {
    return true;
  }

  saveComment() {
    let params = Object.assign({}, this.comment);
    params.body = this.model.body;
    if (!params.body) return;
    this.Mediator.updateComment(params).then(() => {
      this.cancelComment();
    });
  }

  cancelComment() {
    this.model.body = this.comment.body;
    this.comment.resetEditing();
  }
}

EditForm.parameters = [[Comments], [Mediator]];
