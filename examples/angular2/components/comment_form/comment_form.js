import {Component, CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/angular2';

import {Comments} from '../../services/comments';
import {Mediator} from '../../services/mediator';

@Component({
  selector: 'comment-form',
  templateUrl: './components/comment_form/comment_form.html',
  directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
  properties: ['parentType: parent-type', 'parentId: parent-id']
})
export class CommentForm {
  constructor(Comments, Mediator) {
    this.Comments = Comments;
    this.Mediator = Mediator;
    this.model = {
      body: ''
    };
  }

  showCancel() {
    return this.parentType !== 'topic';
  }

  saveComment() {
    let newComment = {
      parentId: this.parentId,
      parentType: this.parentType,
      body: this.model.body.trim()
    };
    if (!newComment.body) return;
    this.Mediator.createComment(newComment).then(() => {
      if(this.showCancel()) this.cancelComment();
    });
  }

  cancelComment() {
    this.model.body = '';
    this.Comments.getById(this.parentId).resetReplying();
  }
}

CommentForm.parameters = [[Comments], [Mediator]];
