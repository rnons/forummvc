import {Component, CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/angular2';
import {RouteParams} from 'angular2/router';

import {Mediator} from '../../services/mediator';
import {Topics} from '../../services/topics';
import {CommentsCom} from '../comments/comments';
import {CommentForm} from '../comment_form/comment_form';

@Component({
  selector: 'component-topic',
  templateUrl: './components/topic/topic.html',
  directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, CommentsCom, CommentForm]
})
export class TopicCom {
  constructor(params, Mediator, Topics) {
    let id = params.get('id');
    this.topic = {};
    Mediator.fetch().then(() => {
      this.topic = Topics.getById(id);
    });
  }
}

TopicCom.parameters = [[RouteParams], [Mediator], [Topics]];
