import {Component, NgFor} from 'angular2/angular2';

import {Mediator} from '../../services/mediator';
import {CommentForm} from '../comment_form/comment_form';
import {EditForm} from '../comment_form/edit_form';

@Component({
  selector: 'comments',
  templateUrl: './components/comments/comments.html',
  directives: [NgFor, CommentsCom, CommentForm, EditForm],
  properties: ['comments']
})
export class CommentsCom {
  constructor(Mediator) {
    this.Mediator = Mediator;
  }

  deleteComment(comment) {
    this.Mediator.deleteComment(comment);
  }
}

CommentsCom.parameters = [[Mediator]];
