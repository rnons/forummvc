import {Component, CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/angular2';
import {RouterLink} from 'angular2/router';

import {Topics} from '../../services/topics';
import {Mediator} from '../../services/mediator';

@Component({
  selector: 'component-home',
  templateUrl: './components/home/home.html',
  directives: [CORE_DIRECTIVES, RouterLink, FORM_DIRECTIVES]
})
export class HomeCom {
  constructor(Topics, Mediator) {
    this.Topics = Topics;
    this.Mediator = Mediator;
    this.model = {
      title: '',
      body: '',
    };
    Mediator.fetch().then(() =>{
      console.log('fetched');
    });
  }

  createTopic() {
    let newTopic = {
      title: this.model.title.trim(),
      body: this.model.body.trim()
    };
    if (!newTopic.title) return;
    this.Mediator.createTopic(newTopic);
    this.model.title = '';
    this.model.body = '';
  }
}

HomeCom.parameters = [[Topics], [Mediator]];
