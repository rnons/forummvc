import 'babelify/polyfill';
import 'reflect-metadata';
import 'zone.js';
import {Component, bootstrap, bind} from 'angular2/angular2';
import {
  ROUTER_PROVIDERS,
  Route,
  RouteConfig,
  RouterOutlet,
  LocationStrategy,
  HashLocationStrategy,
} from 'angular2/router';

import {Topics} from './services/topics';
import {Comments} from './services/comments';
import {Storage} from './services/storage';
import {Mediator} from './services/mediator';
import {HomeCom} from './components/home/home';
import {TopicCom} from './components/topic/topic';

@Component({
  selector: 'forummvc-app',
  templateUrl: 'main.html',
  directives: [RouterOutlet],
  viewBindings: [Topics, Comments, Storage, Mediator]
})
@RouteConfig([
  new Route({ path: '/', component: HomeCom, as: 'Home' }),
  new Route({ path: '/topics/:id', component: TopicCom, as: 'Topic' })
])
class AppComponent {
}

bootstrap(AppComponent, [
  ROUTER_PROVIDERS,
  bind(LocationStrategy).toClass(HashLocationStrategy),
]);
