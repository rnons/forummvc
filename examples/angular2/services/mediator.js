import {Injectable} from 'angular2/angular2';
import {Topics} from './topics';
import {Comments} from './comments';
import {Storage} from './storage';

@Injectable()
export class Mediator {
  constructor(Comments, Storage, Topics) {
    this.TOPICS_KEY = 'topics';
    this.Comments = Comments;
    this.Storage = Storage;
    this.Topics = Topics;
  }

  fetch() {
    return new Promise((resolve) => {
      let data = this.Storage.getItem(this.TOPICS_KEY);
      for (let key in data) {
        this.Topics.add(data[key]);
        for (let params of data[key].comments) {
          this.addComment(params);
        }
      }
      resolve(this.Topics.topics);
    });
  }

  persist() {
    this.Storage.setItem(this.TOPICS_KEY, this.Topics.topics);
  }

  createTopic(params) {
    return new Promise((resolve) => {
      this.Topics.create(params).then((data) => {
        let topic = this.Topics.add(data);
        this.persist();
        resolve(topic);
      });
    });
  }

  createComment(params) {
    return new Promise((resolve) => {
      this.Comments.create(params).then((data) => {
        let comment = this.addComment(data);
        this.persist();
        resolve(comment);
      });
    });
  }

  updateComment(params) {
    return new Promise((resolve) => {
      this.Comments.update(params).then((comment) => {
        this.persist();
        resolve(comment);
      });
    });
  }

  deleteComment(params) {
    return new Promise((resolve) => {
      this.Comments.delete(params).then((comment) => {
        if (comment.parentType === 'topic') {
          this.Topics.getById(comment.parentId).removeComment(comment);
        } else {
          this.Comments.remove(comment);
        }
        delete this.Comments.comments[comment.id];
        this.persist();
        resolve();
      });
    });
  }

  addComment(params) {
    let comment = this.Comments.add(params);
    if (comment.parentType === 'topic') {
      this.Topics.getById(comment.parentId).addComment(comment);
    }
    return comment;
  }
}

Mediator.parameters = [[Comments], [Storage], [Topics]];
