import {Injectable} from 'angular2/angular2';

@Injectable()
export class Storage {
  constructor() {
  }

  getItem(key) {
    return JSON.parse(localStorage.getItem(key) || '{}');
  }

  setItem(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
  }
}
