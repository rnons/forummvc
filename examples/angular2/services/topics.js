import {Injectable} from 'angular2/angular2';

class Topic {
  constructor(params) {
    this.id = params.id,
    this.title = params.title,
    this.body = params.body,
    this.comments = [];
  }

  addComment(comment) {
    this.comments.push(comment);
    return comment;
  }

  removeComment(comment) {
    this.comments.splice(this.comments.indexOf(comment), 1);
  }
}

@Injectable()
export class Topics {
  constructor() {
    this.topics = {};
  }

  create(params) {
    const id = params.id ? params.id : Date.now() + Math.random();
    let topic = {
      id,
      title: params.title,
      body: params.body,
      comments: []
    };
    return Promise.resolve(topic);
  }

  add(params) {
    let topic = new Topic(params);
    this.topics[topic.id] = topic;
    return topic;
  }

  all() {
    let array = [];
    for (let key in this.topics) {
      array.push(this.topics[key]);
    }
    return array;
  }

  getById(id) {
    return this.topics[id];
  }
}
