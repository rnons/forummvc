/* global angular */

angular.module('forummvc.directives', [])

.directive('commentForm', function () {
  return {
    templateUrl: 'comment_form.html',
    restrict: 'E',
    replace: true,
    scope: {
      parentId: '=',
      parentType: '=',
      hideCancel: '='
    },
    controller: function ($scope, Comments, Mediator) {
      $scope.model = {};

      $scope.saveComment = () => {
        Mediator.createComment({
          parentId: $scope.parentId,
          parentType: $scope.parentType,
          body: $scope.model.body
        });
        if (!$scope.hideCancel) $scope.cancelComment();
      };

      $scope.cancelComment = () => {
        Comments.comments[$scope.parentId].replyFormShown = false;
      };
    }
  };
})

.directive('editForm', function () {
  return {
    templateUrl: 'comment_form.html',
    restrict: 'E',
    replace: true,
    scope: {
      comment: '='
    },
    controller: function ($scope, Mediator) {
      $scope.model = {};
      $scope.model.body = $scope.comment.body;

      $scope.saveComment = () => {
        let params = angular.copy($scope.comment);
        params.body = $scope.model.body;
        Mediator.updateComment(params);
        $scope.cancelComment();
      };

      $scope.cancelComment = () => {
        $scope.comment.editFormShown = false;
      };
    }
  };
});
