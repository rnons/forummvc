/* global angular */

angular.module('forummvc.controllers', [])

.controller('MainController', function ($scope, Mediator, Topics) {
  $scope.model = {
    topics: Topics.topics,
    title: '',
    body: ''
  };

  $scope.createTopic = () => {
    let newTopic = {
      title: $scope.model.title.trim(),
      body: $scope.model.body.trim()
    };
    if (!newTopic.title) return;
    Mediator.createTopic(newTopic);
  };

  $scope.deleteTopic = (topic) => {
    Mediator.deleteTopic(topic);
  };
})

.controller('TopicController', function ($routeParams, $scope, Mediator, Topics) {
  $scope.model = {};
  $scope.model.topic = Topics.topics[$routeParams.id];

  $scope.deleteComment = (comment) => {
    Mediator.deleteComment(comment);
  };
});
