/* global angular */

angular.module('forummvc.services')
.factory('Topics', function ($q) {
  class Topic {
    constructor(params) {
      this.id = params.id,
      this.title = params.title,
      this.body = params.body,
      this.comments = [];
    }

    addComment(comment) {
      this.comments.push(comment);
      return comment;
    }

    removeComment(comment) {
      this.comments.splice(this.comments.indexOf(comment), 1);
    }
  }

  return {
    topics: {},

    create(params) {
      let deferred = $q.defer();
      const id = params.id ? params.id : Date.now() + Math.random();
      let topic = {
        id,
        title: params.title,
        body: params.body,
        comments: []
      };
      deferred.resolve(topic);
      return deferred.promise;
    },

    add(params) {
      let topic = new Topic(params);
      this.topics[topic.id] = topic;
      return topic;
    },

    delete(topic) {
      let deferred = $q.defer();
      deferred.resolve(topic);
      return deferred.promise;
    },

    remove(params) {
      delete this.topics[params.id];
    },

    getById(id) {
      return this.topics[id];
    },
  };
});
