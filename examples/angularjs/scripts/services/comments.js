/* global angular */

angular.module('forummvc.services')
.factory('Comments', function ($q) {
  class Comment {
    constructor(params) {
      this.id = params.id,
      this.parentId = params.parentId,
      this.parentType = params.parentType,
      this.body = params.body,
      this.comments = [];
    }
  }

  return {
    comments: {},

    create(params) {
      const id = params.id ? params.id : Date.now() + Math.random();
      let deferred = $q.defer();
      let comment = {
        id: id,
        parentId: params.parentId,
        parentType: params.parentType,
        body: params.body,
        comments: []
      };
      deferred.resolve(comment);
      return deferred.promise;
    },

    update(params) {
      let deferred = $q.defer();
      let comment = this.comments[params.id];
      comment.body = params.body;
      deferred.resolve(comment);
      return deferred.promise;
    },

    delete(params) {
      let deferred = $q.defer();
      let comment = this.comments[params.id];
      deferred.resolve(comment);
      return deferred.promise;
    },

    getById(id) {
      return this.comments[id];
    },

    add(params) {
      let comment = new Comment(params);
      this.comments[comment.id] = comment;
      if (comment.parentType === 'comment') {
        this.comments[comment.parentId].comments.push(comment);
      }
      if (params.comments) {
        for (let data of params.comments) {
          this.add(data);
        }
      }
      return comment;
    },

    remove(comment) {
      let comments = this.comments[comment.parentId].comments;
      comments.splice(comments.indexOf(comment), 1);
    }
  };
});
