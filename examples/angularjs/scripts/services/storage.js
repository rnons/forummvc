/* global angular */

angular.module('forummvc.services')
.factory('Storage', function() {
  return {
    getItem(key) {
      return angular.fromJson(localStorage.getItem(key) || '{}');
    },
    setItem(key, value) {
      localStorage.setItem(key, angular.toJson(value));
    }
  };
});
