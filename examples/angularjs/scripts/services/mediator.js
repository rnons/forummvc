/* global angular */

angular.module('forummvc.services')
.factory('Mediator', function ($q, Comments, Storage, Topics) {
  const TOPICS_KEY = 'topics';

  return {
    fetch() {
      let deferred = $q.defer();
      let data = Storage.getItem(TOPICS_KEY);
      for (let key in data) {
        Topics.add(data[key]);
        for (let params of data[key].comments) {
          this.addComment(params);
        }
      }
      deferred.resolve(Topics.topics);
      return deferred.promise;
    },

    createTopic(params) {
      let deferred = $q.defer();
      Topics.create(params).then((data) => {
        let topic = Topics.add(data);
        Storage.setItem(TOPICS_KEY, Topics.topics);
        deferred.resolve(topic);
      });
      return deferred.promise;
    },

    deleteTopic(topic) {
      let deferred = $q.defer();
      Topics.delete(topic).then((data) => {
        Topics.remove(data);
        Storage.setItem(TOPICS_KEY, Topics.topics);
        deferred.resolve(topic);
      });
      return deferred.promise;
    },

    createComment(params) {
      let deferred = $q.defer();
      Comments.create(params).then((data) => {
        let comment = this.addComment(data);
        Storage.setItem(TOPICS_KEY, Topics.topics);
        deferred.resolve(comment);
      });
      return deferred.promise;
    },

    updateComment(params) {
      let deferred = $q.defer();
      Comments.update(params).then((comment) => {
        Storage.setItem(TOPICS_KEY, Topics.topics);
        deferred.resolve(comment);
      });
      return deferred.promise;
    },

    deleteComment(params) {
      let deferred = $q.defer();
      Comments.delete(params).then((comment) => {
        if (comment.parentType === 'topic') {
          Topics.getById(comment.parentId).removeComment(comment);
        } else {
          Comments.remove(comment);
        }
        delete Comments.comments[comment.id];
        Storage.setItem(TOPICS_KEY, Topics.topics);
        deferred.resolve();
      });
      return deferred.promise;
    },

    addComment(params) {
      let comment = Comments.add(params);
      if (comment.parentType === 'topic') {
        Topics.getById(comment.parentId).addComment(comment);
      }
      return comment;
    }
  };
});
