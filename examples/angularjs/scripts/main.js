/* global angular */

import '../node_modules/angular/angular.js';
import '../node_modules/angular-route/angular-route.js';
import './services.js';
import './directives.js';
import './controllers.js';

angular.module('forummvc', [
  'ngRoute',
  'forummvc.services',
  'forummvc.directives',
  'forummvc.controllers'
])

.config(($routeProvider) => {
  $routeProvider
    .when('/', {
      templateUrl: 'index.html',
      controller: 'MainController',
      controllerAs: 'MainCtrl',
      resolve: {
        _: (Mediator) => Mediator.fetch()
      }
    })
    .when('/topics/:id', {
      templateUrl: 'topic.html',
      controller: 'TopicController',
      controllerAs: 'TopicCtrl',
      resolve: {
        _: (Mediator) => Mediator.fetch()
      }
    })
    .otherwise({
      redirectTo: '/'
    });
});
