/* global describe, beforeEach, inject, it, expect */

describe('MainController', function () {
  let $scope;

  beforeEach(module('forummvc.services'));

  beforeEach(module('forummvc.controllers'));

  beforeEach(inject(($controller, $rootScope, Mediator, Topics) => {
    $scope = $rootScope.$new();
    $controller('MainController', {
      $scope: $scope,
      Mediator: Mediator,
      Topics: Topics
    });
  }));

  it('$scope.model should be an object', () => {
    expect(typeof $scope.model).toBe('object');
  });

  it('$scope.createTopic should be a function', () => {
    expect(typeof $scope.createTopic).toBe('function');
  });

  it('should not createTopic if title is empty', () => {
    expect(Object.keys($scope.model.topics).length).toBe(0);
    $scope.createTopic();
    $scope.$apply();
    expect(Object.keys($scope.model.topics).length).toBe(0);
  });

  it('should not createTopic if title consists of only spaces', () => {
    expect(Object.keys($scope.model.topics).length).toBe(0);
    $scope.model.title = '       ';
    $scope.createTopic();
    $scope.$apply();
    expect(Object.keys($scope.model.topics).length).toBe(0);
  });

  it('should trim title/body in createTopic', () => {
    $scope.model.title = '   new topic    ';
    $scope.model.body = '   good day    ';
    $scope.createTopic();
    $scope.$apply();
    let key = Object.keys($scope.model.topics)[0];
    let topic = $scope.model.topics[key];
    expect(topic.title).toBe('new topic');
    expect(topic.body).toBe('good day');
  });
});

describe('TopicController', function () {
  let $scope;

  beforeEach(module('forummvc.services'));

  beforeEach(module('forummvc.controllers'));

  beforeEach(inject(($controller, $rootScope, Mediator, Topics) => {
    $scope = $rootScope.$new();
    $controller('TopicController', {
      $scope: $scope,
      $routeParams: {id: 42},
      Mediator: Mediator,
      Topics: Topics
    });
  }));

  it('$scope.model should be an object', () => {
    expect(typeof $scope.model).toBe('object');
  });

  it('$scope.deletComment should be a function', () => {
    expect(typeof $scope.deleteComment).toBe('function');
  });
});
