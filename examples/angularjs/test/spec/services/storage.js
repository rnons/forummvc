/* global describe, beforeEach, inject, it, expect */

describe('Storage Service', function () {
  let Storage;

  beforeEach(module('forummvc.services'));

  beforeEach(inject((_Storage_) => {
    Storage = _Storage_;
  }));

  it('should have a getItem function', () => {
    expect(typeof Storage.getItem).toBe('function');
  });

  it('should have a setItem function', () => {
    expect(typeof Storage.setItem).toBe('function');
  });
});
