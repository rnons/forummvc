/* global describe, beforeEach, inject, it, expect */

describe('Comments Service', function () {
  let Comments, $scope;

  beforeEach(module('forummvc.services'));

  beforeEach(inject((_$rootScope_, _Comments_) => {
    Comments = _Comments_;
    $scope = _$rootScope_.$new();
  }));

  it('create should return a promise', (done) => {
    let params = {
      parentId: 1,
      parentType: 'topic',
      body: 'nice article'
    };
    Comments.create(params)
      .then((data) => {
        expect(data.parentId).toBe(params.parentId);
        expect(data.parentType).toBe(params.parentType);
        expect(data.body).toBe(params.body);
      })
      .finally(done);
    $scope.$apply();
  });

  it('create should return different id each time', (done) => {
    let params = {
      parentId: 1,
      parentType: 'topic',
      body: 'nice article'
    };
    let commentOne = null;
    Comments.create(params)
      .then((data) => {
        commentOne = data;
        return Comments.create(params);
      })
      .then((commentTwo) => {
        expect(commentOne.id).not.toEqual(commentTwo.id);
      })
      .finally(done);
    $scope.$apply();
  });

  it('add should return instance of Comment', () => {
    let params = {
      id: 1,
      parentId: 1,
      parentType: 'topic',
      body: 'nice article'
    };
    let comment = Comments.add(params);
    expect(comment.constructor.name).toBe('Comment');
    expect(comment.id).toEqual(params.id);
    expect(comment.parentId).toEqual(params.parentId);
    expect(comment.parentType).toEqual(params.parentType);
    expect(comment.body).toEqual(params.body);
  });

  it('getById should return instance of Comment', function () {
    let params = {
      id: 1,
      parentId: 1,
      parentType: 'topic',
      body: 'nice article'
    };
    let commentAdded = Comments.add(params);
    let comment = Comments.getById(params.id);
    expect(commentAdded).toBe(comment);
    expect(comment.id).toEqual(params.id);
    expect(comment.parentId).toEqual(params.parentId);
    expect(comment.parentType).toEqual(params.parentType);
    expect(comment.body).toEqual(params.body);
  });

  it('update comment body should work', (done) => {
    let params = {
      id: 1,
      parentId: 1,
      parentType: 'topic',
      body: 'nice article'
    };
    let comment = Comments.add(params);
    params.body += '!';
    Comments.update(params)
      .then(() => {
        expect(comment.body).toBe(params.body);
      })
      .finally(done);
    $scope.$apply();
  });

  it('delete should return a promise', (done) => {
    let params = {
      id: 1,
      parentId: 1,
      parentType: 'topic',
      body: 'nice article'
    };
    let comment = Comments.add(params);
    Comments.delete(params)
      .then((data) => {
        expect(data.id).toBe(comment.id);
      })
      .finally(done);
    $scope.$apply();
  });

  it('remove should remove from parent.comments field', () => {
    let paramsOne = {
      id: 1,
      parentId: 1,
      parentType: 'topic',
      body: 'nice article'
    };
    let paramsTwo = {
      id: 2,
      parentId: 1,
      parentType: 'comment',
      body: 'nice comment'
    };
    let commentOne = Comments.add(paramsOne);
    expect(commentOne.comments.length).toBe(0);
    let commentTwo = Comments.add(paramsTwo);
    expect(commentOne.comments.length).toBe(1);
    Comments.remove(commentTwo);
    expect(commentOne.comments.length).toBe(0);
  });

  it('pass return value of create to add should work', (done) => {
    let params = {
      parentId: 1,
      parentType: 'topic',
      body: 'nice article'
    };
    Comments.create(params)
      .then((data) => {
        let comment = Comments.add(data);
        expect(comment.body).toBe(params.body);
      })
      .finally(done);
    $scope.$apply();
  });

  it('pass return value of delete to remove should work', (done) => {
    let paramsOne = {
      id: 1,
      parentId: 1,
      parentType: 'topic',
      body: 'nice article'
    };
    let paramsTwo = {
      id: 2,
      parentId: 1,
      parentType: 'comment',
      body: 'nice comment'
    };
    let commentOne = Comments.add(paramsOne);
    Comments.add(paramsTwo);
    expect(commentOne.comments.length).toBe(1);
    Comments.delete(paramsTwo)
      .then((data) => {
        Comments.remove(data);
        expect(commentOne.comments.length).toBe(0);
      })
      .finally(done);
    $scope.$apply();
  });
});
