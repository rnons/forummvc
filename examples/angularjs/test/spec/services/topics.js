/* global describe, beforeEach, inject, it, expect */

describe('Topics Service', function () {
  let $scope, Topics;

  beforeEach(module('forummvc.services'));

  beforeEach(inject((_$rootScope_, _Topics_) => {
    $scope = _$rootScope_.$new();
    Topics = _Topics_;
  }));

  it('create should return a promise', function (done) {
    let params = {
      title: 'new topic',
      body: 'good day'
    };
    Topics.create(params)
      .then((data) => {
        expect(data.title).toBe(params.title);
        expect(data.body).toBe(params.body);
      })
      .finally(done);
    $scope.$apply();
  });

  it('create should return different id each time', function (done) {
    let params = {
      title: 'new topic',
      body: 'good day'
    };
    let topicOne = null;
    Topics.create(params)
      .then((data) => {
        topicOne = data;
        return Topics.create(params);
      })
      .then((topicTwo) => {
        expect(topicOne.id).not.toEqual(topicTwo.id);
      })
      .finally(done);
    $scope.$apply();
  });

  it('add should return instance of Topic', function () {
    let params = {
      id: '1',
      title: 'new topic',
      body: 'good day'
    };
    let topic = Topics.add(params);
    expect(topic.constructor.name).toBe('Topic');
    expect(topic.id).toEqual(params.id);
    expect(topic.title).toEqual(params.title);
    expect(topic.body).toEqual(params.body);
  });

  it('getById should return instance of Topic', function () {
    let params = {
      id: '1',
      title: 'new topic',
      body: 'good day'
    };
    let topicAdded = Topics.add(params);
    let topic = Topics.getById(params.id);
    expect(topicAdded).toBe(topic);
    expect(topic.constructor.name).toBe('Topic');
    expect(topic.id).toEqual(params.id);
    expect(topic.title).toEqual(params.title);
    expect(topic.body).toEqual(params.body);
  });

  it('addComment should add to topic.comments field', function () {
    let topicParams = {
      id: '1',
      title: 'new topic',
      body: 'good day'
    };
    let commentParams = {
      id: '1',
      parentId: '1',
      parentType: 'topic',
      body: 'tada'
    };
    let topic = Topics.add(topicParams);
    expect(topic.comments.length).toBe(0);
    topic.addComment(commentParams);
    expect(topic.comments.length).toBe(1);
    commentParams.id = 2;
    topic.addComment(commentParams);
    expect(topic.comments.length).toBe(2);
  });

  it('removeComment should remove from topic.comments field', function () {
    let topicParams = {
      id: '1',
      title: 'new topic',
      body: 'good day'
    };
    let commentParams = {
      id: '1',
      parentId: '1',
      parentType: 'topic',
      body: 'tada'
    };
    let topic = Topics.add(topicParams);
    let comment = topic.addComment(commentParams);
    expect(topic.comments.length).toBe(1);
    topic.removeComment(comment);
    expect(topic.comments.length).toBe(0);
  });

  it('pass return value of create to add should work', function (done) {
    let params = {
      title: 'new topic',
      body: 'good day'
    };
    Topics.create(params)
      .then((data) => {
        let topic = Topics.add(data);
        expect(topic.title).toBe(params.title);
        expect(topic.body).toBe(params.body);
      })
      .finally(done);
    $scope.$apply();
  });

  it('pass return value of delete to remove should work', function (done) {
    let params = {
      id: '1',
      title: 'new topic',
      body: 'good day'
    };
    let topic = Topics.add(params);
    expect(Topics.getById(params.id)).toBeDefined();
    Topics.delete(topic)
      .then((data) => {
        Topics.remove(data);
        expect(Topics.getById(params.id)).toBeUndefined();
      })
      .finally(done);
    $scope.$apply();
  });
});
