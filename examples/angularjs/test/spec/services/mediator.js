/* global describe, beforeEach, inject, it, expect, angular */

describe('Mediator Service', function () {
  let $scope, Comments, Topics, Mediator;

  let _Storage_ = {
    getItem() {
      return {
        '1.1': {
            'id': 1.1,
            'title': 'topic one',
            'body': 'a top',
            'comments': [{
                'id': 7.1,
                'parentId': 1.1,
                'parentType': 'topic',
                'body': 'a',
                'comments': [{
                    'id': 8.2,
                    'parentId': 7.1,
                    'parentType': 'comment',
                    'body': 'aa',
                    'comments': []
                }]
            }, {
                'id': 9.3,
                'parentId': 1.1,
                'parentType': 'topic',
                'body': 'b',
                'comments': []
            }]
        },
        '2.2': {
            'id': 2.2,
            'title': 'topic two',
            'body': 'another topic',
            'comments': []
        }
      };
    },
    setItem() {
    }
  };

  beforeEach(module('forummvc.services'));

  beforeEach(() => {
    angular.mock.module(($provide) => {
      $provide.value('Storage', _Storage_);
    });
  });

  beforeEach(inject((_$rootScope_, _Comments_, _Topics_, _Mediator_) => {
    $scope = _$rootScope_;
    Comments = _Comments_;
    Topics = _Topics_;
    Mediator = _Mediator_;
    Mediator.fetch();
  }));

  it('fetch should initialize Topics and Comments', () => {
    expect(Object.keys(Topics.topics).length).toBe(2);
    expect(Object.keys(Comments.comments).length).toBe(3);
  });

  it('createTopic should add to Topics', (done) => {
    let params = {
      title: 'new topic',
      body: 'good day'
    };
    Mediator.createTopic(params)
      .then((topic) => {
        expect(Topics.getById(topic.id).title).toBe(params.title);
        expect(Topics.getById(topic.id).body).toBe(params.body);
      })
      .finally(done);
    $scope.$apply();
  });

  it('deleteTopic should remove from Topics', (done) => {
    let params = {
      title: 'new topic',
      body: 'good day'
    };
    Mediator.createTopic(params)
      .then((topic) => {
        expect(Topics.getById(topic.id)).toBeDefined();
        return Mediator.deleteTopic(topic);
      })
      .then((topic) => {
        expect(Topics.getById(topic.id)).toBeUndefined();
      })
      .finally(done);
    $scope.$apply();
  });

  it('createComment should add to Comments', (done) => {
    let params = {
      parentId: 2.2,
      parentType: 'topic',
      body: 'nice article'
    };
    Mediator.createComment(params)
      .then((comment) => {
        expect(Comments.getById(comment.id).parentId).toBe(params.parentId);
        expect(Comments.getById(comment.id).parentType).toBe(params.parentType);
        expect(Comments.getById(comment.id).body).toBe(params.body);
      })
      .finally(done);
    $scope.$apply();
  });

  it('updateComment should update comment.body', (done) => {
    let comment = Comments.getById(8.2);
    let originalBody = comment.body;
    let params = angular.copy(comment);
    params.body = originalBody + '!';
    Mediator.updateComment(params)
      .then((comment) => {
        expect(Comments.getById(comment.id).body).not.toBe(originalBody);
        expect(Comments.getById(comment.id).body).toBe(params.body);
      })
      .finally(done);
    $scope.$apply();
  });

  it('deleteComment of comment should remove from Comments', (done) => {
    let comment = Comments.getById(8.2);
    let comments = Comments.getById(comment.parentId).comments;
    let length = comments.length;
    Mediator.deleteComment(comment)
      .then(() => {
        expect(Comments.getById(8.2)).toBeUndefined();
        expect(comments.length).toBe(length - 1);
      })
      .finally(done);
    $scope.$apply();
  });

  it('deleteComment of topic should also remove from Topics', (done) => {
    let comment = Comments.getById(7.1);
    let comments = Topics.getById(comment.parentId).comments;
    let length = comments.length;
    Mediator.deleteComment(comment)
      .then(() => {
        expect(Comments.getById(7.1)).toBeUndefined();
        expect(comments.length).toBe(length - 1);
      })
      .finally(done);
    $scope.$apply();
  });

  it('addComment to comment should add to Comments', () => {
    let params = {
      id: 10.4,
      parentId: 9.3,
      parentType: 'comment',
      body: 'comment to 9.3'
    };
    Mediator.addComment(params);
    expect(Comments.getById(params.id)).toBeDefined();
  });

  it('addComment to topic should also add to Topics', () => {
    let params = {
      id: 10.4,
      parentId: 2.2,
      parentType: 'topic',
      body: 'comment to 2.2'
    };
    let topic = Topics.getById(2.2);
    let length = topic.comments.length;
    Mediator.addComment(params);
    expect(Comments.getById(params.id)).toBeDefined();
    expect(topic.comments.length).toBe(length + 1);
  });
});
