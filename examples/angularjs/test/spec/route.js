/* global describe, beforeEach, inject, it, expect */

describe('Route', function () {
  let $location, $route, $scope;

  beforeEach(module('forummvc'));

  beforeEach(inject((_$location_, _$route_, _$rootScope_) => {
    $location = _$location_;
    $route = _$route_;
    $scope = _$rootScope_;
  }));

  beforeEach(inject(($httpBackend) => {
    $httpBackend.whenGET('index.html').respond('index page');
    $httpBackend.whenGET('topic.html').respond('topic page');
  }));

  it('should load index.html and MainController on /', () => {
    $location.path('/');
    $scope.$apply();
    expect($route.current.loadedTemplateUrl).toBe('index.html');
    expect($route.current.controller).toBe('MainController');
  });

  it('should load topic.html and TopicController on /', () => {
    $location.path('/topics/42');
    $scope.$apply();
    expect($route.current.loadedTemplateUrl).toBe('topic.html');
    expect($route.current.controller).toBe('TopicController');
  });

  it('should redirect to index on undefined route', () => {
    $location.path('/topics');
    $scope.$apply();
    expect($route.current.loadedTemplateUrl).toBe('index.html');
    expect($route.current.controller).toBe('MainController');
  });
});
