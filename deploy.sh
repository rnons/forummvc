#!/bin/sh
rm -rf dist
NODE_ENV=production gulp build

rm -rf .tmp
mkdir .tmp
git clone git@bitbucket.org:rnons/rnons.bitbucket.org.git .tmp

rm -rf .tmp/forummvc
cp -r dist/forummvc .tmp/forummvc

cd .tmp
git add .
git commit -m 'deploy'
git push

cd ..
rm -rf .tmp
