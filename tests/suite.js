var assert = require('assert');
var test = require('selenium-webdriver/testing');
var webdriver = require('selenium-webdriver');
var By = webdriver.By;

var driver;

module.exports.test = function(framework) {
  test.describe('homepage', () => {
    test.before(() => {
      driver = new webdriver.Builder()
        .forBrowser('firefox')
        .build();
    });

    test.beforeEach(() => {
      driver.get('http://localhost:9000/forummvc/' + framework);
    });

    test.after(() => {
      driver.quit();
    });

    test.it('create button should be disabled', () => {
      var $button = driver.findElement(By.css('input[type="submit"]'));
      $button.getAttribute('disabled').then((disabled) => {
        assert.equal(disabled, 'true');
      });
    });

    test.it('should have no topic', () => {
      driver.findElements(By.className('Topic')).then((elements) => {
        assert.equal(elements.length, 0);
      });
    });

    test.it('should create a new topic', () => {
      var $title, $body, $button;
      $title = driver.findElement(By.className('Form-topic-title'));
      $body = driver.findElement(By.className('Form-topic-body'));
      $button = driver.findElement(By.css('input[type="submit"]'));
      $title.sendKeys('a new topic');
      $body.sendKeys('good day');
      $button.click();
      driver.findElements(By.className('Topic')).then((elements) => {
        assert.equal(elements.length, 1);
        elements[0].getText().then((text) => {
          assert.equal(text, 'a new topic');
        });
      });
    });

    test.it('should create another topic', () => {
      var $title, $body, $button;
      $title = driver.findElement(By.className('Form-topic-title'));
      $body = driver.findElement(By.className('Form-topic-body'));
      $button = driver.findElement(By.css('input[type="submit"]'));
      $title.sendKeys('another new topic');
      $body.sendKeys('another good day');
      $button.click();
      driver.findElements(By.className('Topic')).then((elements) => {
        assert.equal(elements.length, 2);
        elements[1].getText().then((text) => {
          assert.equal(text, 'another new topic');
        });
      });
    });

    test.it('should delete the first topic', () => {
      driver.findElements(By.className('Topic-delete')).then(($delete) => {
        driver.executeScript('arguments[0].click()', $delete[0]);
        driver.findElements(By.className('Topic')).then((elements) => {
          assert.equal(elements.length, 1);
          elements[0].getText().then((text) => {
            assert.equal(text, 'another new topic');
          });
        });
      });
    });

    test.it('should delete the other topic', () => {
      driver.findElements(By.className('Topic-delete')).then(($delete) => {
        driver.executeScript('arguments[0].click()', $delete[0]);
        driver.findElements(By.className('Topic')).then((elements) => {
          assert.equal(elements.length, 0);
        });
      });
    });
  });
};
