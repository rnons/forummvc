#!/usr/bin/env node

var argv = require('yargs').argv;
var fs = require('fs');
var suite = require('./suite');

var framework = argv.framework;

var list = fs.readdirSync('./examples/');
if (list.indexOf(framework) === -1) {
  throw new Error('Unknow framework');
}

suite.test(framework);
