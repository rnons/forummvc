import gulp from 'gulp';
import babelify from 'babelify';
import browserify from 'browserify';
import buffer from 'vinyl-buffer';
import connect from 'connect';
import cssnext from 'gulp-cssnext';
import gulpIf from 'gulp-if';
import eslint from 'gulp-eslint';
import ngAnnotate from 'gulp-ng-annotate';
import source from 'vinyl-source-stream';
import uglify from 'gulp-uglify';
import webserver from 'gulp-webserver';

const STATIC = './dist';
const REPO = 'forummvc';
const DEST = `${STATIC}/${REPO}`;
const isProd = (process.env.NODE_ENV === 'production') ? true : false;

gulp.task('styles', () => {
  return gulp.src('assets/styles/main.css', {base: 'assets'})
    .pipe(cssnext({
      compress: true
    }))
    .pipe(gulp.dest(DEST));
});

gulp.task('lint', () => {
  return gulp.src([
    'examples/**/*.js',
    '!examples/**/node_modules/**/*.js'
    ])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failOnError());
});

gulp.task('scripts', isProd ? ['lint'] : null, () => {
  let scripts = [
    'examples/angularjs/scripts/main.js',
    'examples/vanillajs/scripts/main.js',
    'examples/angular2/main.js',
    'examples/react/scripts/main.js',
  ];
  let output = (src) => {
    let dest = src.split('/');
    dest.shift();
    return dest.join('/');
  };
  let isAngular = (file) => file.relative === 'angularjs/scripts/main.js';

  scripts.forEach((script) => {
    browserify(script, {debug: true})
      .transform(babelify.configure({
        optional: ['es7.decorators']
      }))
      .bundle()
      .on('error', function (err) { console.log('Error : ' + err.message); })
      .pipe(source(output(script)))
      .pipe(buffer())
      .pipe(gulpIf(isAngular, ngAnnotate()))
      .pipe(gulpIf(isProd, uglify()))
      .pipe(gulp.dest(DEST));
  });
});

gulp.task('html', () => {
  return gulp.src([
    'examples/**/*.html',
    '!examples/**/node_modules/**/*.html'
    ])
    .pipe(gulp.dest(DEST));
});

gulp.task('default', ['styles', 'scripts', 'html']);

gulp.task('serve', ['default'], () => {
  gulp.src(STATIC)
    .pipe(webserver({
      port: 9000,
      livereload: true,
    }));

  gulp.watch(['assets/styles/**/*.css'], ['styles']);
  gulp.watch(['examples/**/*.js', '!examples/**/node_modules/**/*.js'], ['scripts']);
  gulp.watch(['examples/**/*.html'], ['html']);
});

gulp.task('build', ['default'], () => {
  if (!isProd) {
    throw new Error('Requires NODE_ENV set to production, run `NODE_ENV=production gulp build`');
  }
});
